namespace :db do
  task :populate => :environment do
    require 'populator'
    require 'faker'
    password = 'password'
    [User, Favor].each(&:delete_all)

    User.populate 5 do |user|
      user.email = Faker::Internet.email
      user.firstname = Faker::Name.name.split(" ").first
      user.lastname = Faker::Name.name.split(" ").last
      user.sign_in_count = [5, 2, 15]
      user.encrypted_password = User.new(:password => password).encrypted_password
      user.address ='Budapest'
      user.latitude = 47.4925
      user.longitude = 19.0514
      Favor.populate 3 do |favor|
        favor.favor_to = user.id
        favor.place = ['Arena mall', 'City Center', 'Allee Mall' ]
        favor.description = ['Pick up Medicine', 'Drop off bag', 'Moving day']
      end
      Favor.populate 1 do |favor|
        favor.favor_from = user.id
        favor.place = ['Arena mall', 'City Center', 'Allee Mall' ]
        favor.description = ['Pick up Medicine', 'Drop off bag', 'Moving day']
      end
    end

    User.populate 5 do |user|
      user.email = Faker::Internet.email
      user.firstname = Faker::Name.name.split(" ").first
      user.lastname = Faker::Name.name.split(" ").last
      user.sign_in_count = [5, 2, 15]
      user.encrypted_password = User.new(:password => password).encrypted_password
      user.address ='Vienna'
      user.latitude = 48.2000
      user.longitude = 16.3667
      Favor.populate 3 do |favor|
        favor.favor_to = user.id
        favor.place = ['Shcafor Mall', 'City Center', 'Echlisfen']
        favor.description = ['Pick up Medicine', 'Drop off bag', 'Moving day']
      end
      Favor.populate 1 do |favor|
        favor.favor_from = user.id
      end
    end
  end
end