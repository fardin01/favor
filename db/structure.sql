CREATE TABLE "schema_migrations" ("version" varchar NOT NULL);
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
CREATE TABLE "rewards" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar, "description" text, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "favors" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "place" varchar, "description" text, "meet" boolean, "need_money" boolean, "favor_length" integer, "physical_work" boolean, "favor_from" integer, "favor_to" integer, "user_id" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "favors_rewards" ("reward_id" integer, "favor_id" integer);
CREATE INDEX "index_favors_rewards_on_reward_id" ON "favors_rewards" ("reward_id");
CREATE INDEX "index_favors_rewards_on_favor_id" ON "favors_rewards" ("favor_id");
CREATE TABLE "users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar DEFAULT '' NOT NULL, "encrypted_password" varchar DEFAULT '' NOT NULL, "reset_password_token" varchar, "reset_password_sent_at" datetime, "remember_created_at" datetime, "sign_in_count" integer DEFAULT 0 NOT NULL, "current_sign_in_at" datetime, "last_sign_in_at" datetime, "current_sign_in_ip" varchar, "last_sign_in_ip" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "firstname" varchar, "lastname" varchar, "home_town" varchar, "languages" varchar, "phone" integer, "bio" text, "points" integer, "skills" varchar, "admin" boolean, "image" varchar, "username" varchar, "name" varchar, "latitude" float, "longitude" float, "address" varchar);
CREATE UNIQUE INDEX "index_users_on_email" ON "users" ("email");
CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" ("reset_password_token");
CREATE TABLE "favor_requests" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "favor_id" integer, "accepted" boolean DEFAULT 'f', "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "user_id" integer);
CREATE TABLE "activities" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "user_id" integer, "action" varchar, "trackable_id" integer, "trackable_type" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_activities_on_user_id" ON "activities" ("user_id");
CREATE INDEX "index_activities_on_trackable_id" ON "activities" ("trackable_id");
CREATE UNIQUE INDEX "index_favor_requests_on_user_id_and_favor_id" ON "favor_requests" ("user_id", "favor_id");
INSERT INTO schema_migrations (version) VALUES ('20151123184007');

INSERT INTO schema_migrations (version) VALUES ('20151128181118');

INSERT INTO schema_migrations (version) VALUES ('20151128185427');

INSERT INTO schema_migrations (version) VALUES ('20151128191235');

INSERT INTO schema_migrations (version) VALUES ('20151129202129');

INSERT INTO schema_migrations (version) VALUES ('20151130180710');

INSERT INTO schema_migrations (version) VALUES ('20151204190303');

INSERT INTO schema_migrations (version) VALUES ('20151204192716');

INSERT INTO schema_migrations (version) VALUES ('20151204210358');

INSERT INTO schema_migrations (version) VALUES ('20151204210415');

INSERT INTO schema_migrations (version) VALUES ('20151205165839');

INSERT INTO schema_migrations (version) VALUES ('20151205180934');

INSERT INTO schema_migrations (version) VALUES ('20151210223634');

INSERT INTO schema_migrations (version) VALUES ('20151210224449');

INSERT INTO schema_migrations (version) VALUES ('20151210230038');

INSERT INTO schema_migrations (version) VALUES ('20151211210107');

INSERT INTO schema_migrations (version) VALUES ('20151212183325');

INSERT INTO schema_migrations (version) VALUES ('20151212184843');

INSERT INTO schema_migrations (version) VALUES ('20151212190640');

INSERT INTO schema_migrations (version) VALUES ('20151212190925');

INSERT INTO schema_migrations (version) VALUES ('20151212191208');

INSERT INTO schema_migrations (version) VALUES ('20151213160010');

INSERT INTO schema_migrations (version) VALUES ('20151213160623');

INSERT INTO schema_migrations (version) VALUES ('20151213222659');

INSERT INTO schema_migrations (version) VALUES ('20151213223653');

INSERT INTO schema_migrations (version) VALUES ('20151213224533');

INSERT INTO schema_migrations (version) VALUES ('20151219144242');

INSERT INTO schema_migrations (version) VALUES ('20151219145331');

INSERT INTO schema_migrations (version) VALUES ('20151219153405');

INSERT INTO schema_migrations (version) VALUES ('20151219190414');

INSERT INTO schema_migrations (version) VALUES ('20151225185520');

INSERT INTO schema_migrations (version) VALUES ('20151228211038');

INSERT INTO schema_migrations (version) VALUES ('20151228212309');

INSERT INTO schema_migrations (version) VALUES ('20151228213700');

