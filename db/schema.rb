# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160109141004) do

  create_table "activities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "action"
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "activities", ["trackable_id"], name: "index_activities_on_trackable_id"
  add_index "activities", ["user_id"], name: "index_activities_on_user_id"

  create_table "favor_requests", force: :cascade do |t|
    t.integer  "favor_id"
    t.boolean  "accepted",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "user_id"
  end

  add_index "favor_requests", ["user_id", "favor_id"], name: "index_favor_requests_on_user_id_and_favor_id", unique: true

  create_table "favors", force: :cascade do |t|
    t.string   "place"
    t.text     "description"
    t.boolean  "meet"
    t.boolean  "need_money"
    t.integer  "favor_length"
    t.boolean  "physical_work"
    t.integer  "favor_from"
    t.integer  "favor_to"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "completed"
  end

  create_table "favors_rewards", id: false, force: :cascade do |t|
    t.integer "reward_id"
    t.integer "favor_id"
  end

  add_index "favors_rewards", ["favor_id"], name: "index_favors_rewards_on_favor_id"
  add_index "favors_rewards", ["reward_id"], name: "index_favors_rewards_on_reward_id"

  create_table "rewards", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "firstname"
    t.string   "lastname"
    t.string   "home_town"
    t.string   "languages"
    t.integer  "phone"
    t.text     "bio"
    t.integer  "points"
    t.string   "skills"
    t.boolean  "admin"
    t.string   "image"
    t.string   "username"
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "address"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
