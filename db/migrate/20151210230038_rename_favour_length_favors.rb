class RenameFavourLengthFavors < ActiveRecord::Migration
  def change
    rename_column :favors, :favour_length, :favor_length 
  end
end
