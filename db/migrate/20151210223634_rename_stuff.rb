class RenameStuff < ActiveRecord::Migration
  def change

    rename_column :favours, :favour_to, :favor_to
    rename_column :favours, :favour_from, :favor_from

  end
end
