class ChangeStuffInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :first_name, :firstname
    rename_column :users, :last_name, :lastname
    rename_column :users, :phone_number, :phone
    add_column :users, :name, :string
  end
end
