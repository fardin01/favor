class RenameFavourIdToFavorIdInFavorsRewards < ActiveRecord::Migration
  def change
    rename_column :favors_rewards, :favour_id, :favor_id
  end
end
