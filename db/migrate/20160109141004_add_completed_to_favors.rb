class AddCompletedToFavors < ActiveRecord::Migration
  def change
    add_column :favors, :completed, :boolean
  end
end
