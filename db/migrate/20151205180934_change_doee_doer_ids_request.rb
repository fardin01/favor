class ChangeDoeeDoerIdsRequest < ActiveRecord::Migration
  def change
    rename_column :requests, :doee_id, :favour_to
    rename_column :requests, :doer_id, :favour_from 
  end
end
