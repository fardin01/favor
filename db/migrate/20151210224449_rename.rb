class Rename < ActiveRecord::Migration
  def change

    rename_column :requests, :favour_id, :favor_id

    rename_table :favours, :favors
    rename_table :favours_rewards, :favors_rewards

  end
end
