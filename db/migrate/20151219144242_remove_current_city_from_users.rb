class RemoveCurrentCityFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :current_city
    add_column :users, :latitude, :float
    add_column :users, :longtitude, :float
  end
end
