class ModifyRequest < ActiveRecord::Migration
  def change
    remove_column :requests, :favor_to
    remove_column :requests, :favor_from
    add_column :requests, :user_id, :integer
  end
end
