class ChangeDoeeDoerIds < ActiveRecord::Migration
  def change
    rename_column :favours, :doee_id, :favour_to
    rename_column :favours, :doer_id, :favour_from
  end
end
