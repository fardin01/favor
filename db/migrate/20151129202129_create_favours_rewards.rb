class CreateFavoursRewards < ActiveRecord::Migration
  def change
    create_table :favours_rewards do |t|
      t.references :reward, index: true
      t.references :favour, index: true
    end
  end
end
