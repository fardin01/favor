class CreateRequests < ActiveRecord::Migration
  def change
    create_table :favorequests do |t|
      t.integer :doee_id
      t.integer :doer_id
      t.integer :favour_id
      t.boolean :accepted

      t.timestamps null: false
    end
  end
end
