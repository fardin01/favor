class RemoveIdFromFavoursReward < ActiveRecord::Migration
  def change
    remove_column :favours_rewards, :id
  end
end
