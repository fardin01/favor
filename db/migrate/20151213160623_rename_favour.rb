class RenameFavour < ActiveRecord::Migration
  def change
    rename_column :requests, :favour_to, :favor_to
    rename_column :requests, :favour_from, :favor_from
  end
end
