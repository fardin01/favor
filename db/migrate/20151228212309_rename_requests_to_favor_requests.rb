class RenameRequestsToFavorRequests < ActiveRecord::Migration
  def change
    rename_table :requests, :favor_requests
  end
end
