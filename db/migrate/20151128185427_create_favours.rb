class CreateFavours < ActiveRecord::Migration
  def change
    create_table :favours do |t|

      t.string  :place
      t.text    :description
      t.integer :reward_ids, array: true, default: []
      t.boolean :meet
      t.boolean :need_money
      t.integer :favour_length
      t.boolean :physical_work
      t.integer :doer_id
      t.integer :doee_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
