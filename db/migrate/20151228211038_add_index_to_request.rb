class AddIndexToRequest < ActiveRecord::Migration
  def change
    add_index :requests, [:user_id, :favor_id], unique: true
  end
end
