class RenameLongtitudeUsers < ActiveRecord::Migration
  def change
    rename_column :users, :longtitude, :longitude
  end
end
