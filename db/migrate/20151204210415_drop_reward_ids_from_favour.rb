class DropRewardIdsFromFavour < ActiveRecord::Migration
  def change
    remove_column :favours, :reward_ids
  end
end
