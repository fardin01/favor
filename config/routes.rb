Rails.application.routes.draw do
  root to: 'site#index'
  devise_for :users 

  resources :activities
  resources :users
  resources :favors
  get 'ask-favor' => 'favors#new'
  get 'publish-favor' => 'favors#new'

  resources :favor_requests, except: [:index, :show, :new]

end
