include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :favor do
    sequence(:place) {|n| "City no. #{n}"}
    sequence(:description) {|n| "description no #{n}"}
    
    # Undefined method "rand="
    #favor_length rand(1..8)  

    after(:create) do |favor|
      favor.rewards << FactoryGirl.create(:reward)
    end 
  end

  factory :reward do
    title ["Cash", "Favor", "Food", "Dance lesson"].sample
    sequence(:description) {|n| "Reward no. #{n}"}
  end

  factory :user do
    sequence(:firstname)
    sequence(:lastname)
    sequence(:email)
    password "password"
    address "Budapest"
    latitude 47.4925
    longitude 19.0514
  end
end