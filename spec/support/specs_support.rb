module SpecsHelper
  
  def sign_in
    visit new_user_session_path
    fill_in 'user_login', with: "Fardin01"
    fill_in 'user_password', with: '123'
    click_button 'Log in'
    expect(page).to have_content ('Signed in successfully')
  end

end