require "rails_helper"

feature "Favors" do 
  feature "Creating a Favor" do
    before do
      @favor = create :favor, place: "HH"
    end

    scenario "User views a favor" do
      sign_in
      visit favor_path(@favor)
      expect(page).to have_content "HH"
    end 
  end
end