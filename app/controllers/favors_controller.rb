class FavorsController < ApplicationController
  
  def index
    #Index page shows the favors based on their user's address comparing to
    #current user address
    @near_users = current_user.nearbys(30)
  end

  def show
    @favor = Favor.find(params[:id])
  end

  def new
    @favor = Favor.new
  end

  def create
    @favor = Favor.create(favor_params)
    if @favor.save
      flash[:notice] = "favor successfully created"
      redirect_to favors_path
    else
      render 'new'
    end
  end
  private

  def favor_params
    params.require(:favor).permit(:user_id, :favor_to, :favor_from, :place, :description, 
                      :favor_length, :meet, :need_money, :physical_work, reward_ids: [])
  end

end