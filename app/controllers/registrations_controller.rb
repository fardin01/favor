class RegistrationsController < Devise::RegistrationsController
  after_action :user_full_name, only: :create

  private

  def user_full_name
    @user.address = "Name Test"
  end
end