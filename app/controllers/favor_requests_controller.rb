class FavorRequestsController < ApplicationController
  include FavorsHelper
  before_action :find_favor_request, only: :update

  def create
    @favor_request = FavorRequest.create(favor_request_params)
    
    track_activity(@favor_request)
    if @favor_request.save
      flash[:notice] = "Your favor request has been sent"
      redirect_to favors_path
    else
      render :template => "favors/index" 
    end
  end

  def update
    @favor_request.update_attribute(:accepted, true)
    track_activity(@favor_request)

    if published_favor?(@favor_request.favor)
      @favor_request.favor.update_attribute(:favor_to, @favor_request.user_id)
    elsif asked_favor?(@favor_request.favor)
      @favor_request.favor.update_attribute(:favor_from, @favor_request.user_id)
    end

    if @favor_request.save
      redirect_to favor_path(@favor_request.favor)
    else
      render :template => "favors/show" 
    end
  end

  def destroy
  end

  private

  def find_favor_request
    @favor_request = FavorRequest.find(params[:id])
  end

  def favor_request_params
    params.require(:favor_request).permit(:user_id, :favor_id)
  end
end