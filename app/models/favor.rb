# == Schema Information
#
# Table name: favors
#
#  id            :integer          not null, primary key
#  place         :string
#  description   :text
#  meet          :boolean
#  need_money    :boolean
#  favor_length  :integer
#  physical_work :boolean
#  favor_from    :integer
#  favor_to      :integer
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Favor < ActiveRecord::Base


  belongs_to :favor_asker, class_name: 'User', foreign_key: 'favor_to' 
  belongs_to :favor_doer, class_name: 'User', foreign_key: 'favor_from'


  has_many :favors_rewards
  has_many :rewards, through: :favors_rewards

  has_many :favor_requests
  
end
