# == Schema Information
#
# Table name: favor_requests
#
#  id         :integer          not null, primary key
#  favor_id   :integer
#  accepted   :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class FavorRequest < ActiveRecord::Base
  belongs_to :favor
  belongs_to :user


end
