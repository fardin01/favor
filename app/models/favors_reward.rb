# == Schema Information
#
# Table name: favors_rewards
#
#  reward_id :integer
#  favor_id  :integer
#

class FavorsReward < ActiveRecord::Base
  belongs_to :favor
  belongs_to :reward
end
