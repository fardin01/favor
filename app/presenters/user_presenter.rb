class UserPresenter

  def initialize(user, template)
    @user = user
    @template = template
  end

  def h
    @template
  end

  def published_uncompleted_favors
    @user.favors_done.map do |favor|
      next if favor.completed
      next if done_favors(favor)
      h.link_to(favor.description, h.favor_path(favor))
    end
  end

  # If favor_from != favor_to && user_id then it's a done favor
  # and not a published favor
  def done_favors(favor)
    favor.favor_from != favor.favor_to && favor.favor_from != favor.user_id
  end
end