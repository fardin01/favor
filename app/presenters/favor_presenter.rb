class FavorPresenter
  def initialize(favor, template)
    @favor = favor
    @template = template
  end

  def h
    @template
  end

  # Two below methods are to be used in favors view
  # Same methods are avaialbe in favors helper to be used in favors controller
  def published_favor?
    @favor.favor_from == @favor.user_id
  end

  def asked_favor?
    @favor.favor_to == @favor.user_id
  end

  def favor_request_accepted?
    @favor.favor_requests.any?{ |request| request.accepted }
  end

  def accepted_favor_request
    @favor.favor_requests.last if favor_request_accepted?
  end

  # Index page
  def title
     h.link_to('Favor', h.favor_path(@favor)) + asked_or_published? + user_link
  end

  def user_link
    if asked_favor?
      h.link_to @favor.favor_asker.firstname, h.user_path(@favor.favor_asker)
    elsif published_favor?
      h.link_to @favor.favor_doer.firstname, h.user_path(@favor.favor_doer)
    end
  end

  def asked_or_published?
    if asked_favor?
      h.content_tag(:p, "asked by")
    elsif published_favor?
      h.content_tag(:p, "published by")
    end 
  end

  def favor_request
    # Use JS to disable links
    if asked_favor?
      h.link_to "Do this Favor", h.favor_requests_path(favor_request: {user_id: h.current_user.id, favor_id: @favor.id}), method: :post 
    elsif published_favor?
      h.link_to "Ask this Favor", h.favor_requests_path(favor_request: {user_id: h.current_user.id, favor_id: @favor.id}), method: :post 
    end
  end

  def rewards
    @favor.rewards.map{ |reward| reward.title }.join(", ")
  end

  # Show page
  def accepted_request_message
    if asked_favor? 
      h.link_to(@favor.favor_requests.last.user.firstname, h.user_path(@favor.favor_requests.last.user)) + " has agreed to do you this favor"
    elsif published_favor?
      "You agreed to do " + h.link_to(@favor.favor_requests.last.user.firstname, h.user_path(@favor.favor_requests.last.user )) + " this favor"
    end
  end


  def unaccepted_requests_message
    if @favor.favor_requests.empty?
      if asked_favor?
        "No one has offered to do you this favor yet!"
      elsif published_favor?
        "No one needs this favor yet!"
      end
    elsif asked_favor?
      "The following fellas have offered to do you this favor:"
    elsif published_favor?
      "The following fellas need this favor from you:"
    end
  end
  def requests
    @favor.favor_requests.map do |favor_request| 
       h.link_to(favor_request.user.firstname, h.user_path(favor_request.user)) + " " + h.link_to('Accept', h.favor_request_path(favor_request.id), method: :put)
    end
  end
end