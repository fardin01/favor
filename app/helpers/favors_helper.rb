module FavorsHelper 

  def ask_favor_page
    current_page?(ask_favor_path)
  end

  def publish_favor_page
    current_page?(publish_favor_path)
  end

  # The below methods are to be used in favors controller
  # Same methods are available in favor presenter to be used in favor view
  def published_favor?(favor)
    favor.favor_from == favor.user_id
  end

  def asked_favor?(favor)
    favor.favor_to == favor.user_id
  end
end